package de.dieunkreativen.eastereggs;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import de.dieunkreativen.eastereggs.util.InventoryUtil;

public class DBHandler {
	
	public static void createTables() {
		
		 String EasterEggsData = "CREATE TABLE IF NOT EXISTS EasterEggsData ("
		       		+ "id INTEGER PRIMARY KEY AUTOINCREMENT,"
		       		+ " inventory CHAR(100) DEFAULT NULL,"
		       		+ " x int(5) DEFAULT NULL,"
		       		+ " y int(5) DEFAULT NULL,"
		       		+ " z int(5) DEFAULT NULL,"
		  			+ " world CHAR(30) DEFAULT NULL)";
		 
		 String PlayerData = "CREATE TABLE IF NOT EXISTS PlayerData ("
		       		+ "id INTEGER PRIMARY KEY AUTOINCREMENT,"
		       		+ " uuid CHAR(50) DEFAULT NULL,"
		       		+ " x int(5) DEFAULT NULL,"
		       		+ " y int(5) DEFAULT NULL,"
		       		+ " z int(5) DEFAULT NULL,"
		  			+ " world CHAR(30) DEFAULT NULL)";

	        try {
	        	java.sql.Statement statement = EasterEggs.c.createStatement();
	        	statement.executeUpdate(EasterEggsData);
	        	statement.executeUpdate(PlayerData);
	        	statement.close();
	        }
	        catch (SQLException e) {
	        	e.printStackTrace();            
	        }
	}
	
	public static void addOpenedEgg(Player p, Location loc) {
	    try {
    		java.sql.Statement statement = EasterEggs.c.createStatement();
    		statement.executeUpdate("INSERT INTO PlayerData "
    				+ "(`uuid`,  `x`, `y`, `z`, `world`) "
    				+ "VALUES ('" + p.getUniqueId().toString() + "', '" + loc.getX() + "', '" + loc.getY() + "', "
    				+ "'" + loc.getZ() + "', '" + loc.getWorld().getName() + "');");
    		statement.close();
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
	}
	
	public static boolean hasOpened(Player p, Location loc) {
		boolean result = true;
		try {
			java.sql.Statement st = EasterEggs.c.createStatement();
			String sql = ("SELECT * FROM PlayerData WHERE uuid='" + p.getUniqueId().toString() +  "'AND x='" + loc.getX() + "'AND y='" + loc.getY() 
					+ "'AND z='" + loc.getZ() + "'AND world='" + loc.getWorld().getName() + "';");
			ResultSet rs = st.executeQuery(sql);
			if (!rs.isBeforeFirst() ) {  
				result = false;
			}
			rs.close();
			st.close();
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
		return result;
	}
	
	public static void addEasterEgg(Inventory inv, Location loc) {
	    try {
    		java.sql.Statement statement = EasterEggs.c.createStatement();
    		statement.executeUpdate("INSERT INTO EasterEggsData "
    				+ "(`inventory`,  `x`, `y`, `z`, `world`) "
    				+ "VALUES ('" + InventoryUtil.toBase64(inv) + "', '" + loc.getX() + "', '" + loc.getY() + "', "
    				+ "'" + loc.getZ() + "', '" + loc.getWorld().getName() + "');");
    		statement.close();
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
	}
	
	public static void removeEasterEgg(Location loc) {
		try {
			java.sql.Statement st = EasterEggs.c.createStatement();
			st.executeUpdate("DELETE FROM EasterEggsData WHERE x='" + loc.getX() + "'AND y='" + loc.getY() 
					+ "'AND z='" + loc.getZ() + "'AND world='" + loc.getWorld().getName() + "';");
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
	}
	
	public static Inventory getEasterEggInventory(Location loc) {
		Inventory inv = null;
		try {
			java.sql.Statement st = EasterEggs.c.createStatement();
			String sql = ("SELECT inventory FROM EasterEggsData WHERE x='" + loc.getX() + "'AND y='" + loc.getY() 
					+ "'AND z='" + loc.getZ() + "'AND world='" + loc.getWorld().getName() + "';");
			ResultSet rs = st.executeQuery(sql);
			try {
				inv = InventoryUtil.fromBase64(rs.getString("inventory"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//Bukkit.broadcastMessage("DB " + rs.getString("inventory"));
			rs.close();
			st.close();
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
		return inv;
	}
	
	public static boolean isEasterEgg(Location loc) {
		boolean result = true;
		try {
			java.sql.Statement st = EasterEggs.c.createStatement();
			String sql = ("SELECT * FROM EasterEggsData WHERE x='" + loc.getX() + "'AND y='" + loc.getY() 
					+ "'AND z='" + loc.getZ() + "'AND world='" + loc.getWorld().getName() + "';");
			ResultSet rs = st.executeQuery(sql);
			if (!rs.isBeforeFirst() ) {  
				result = false;
			}
			rs.close();
			st.close();
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
		return result;
	}
	
	public static int countMaxEasterEggs() {
		int result = 555;
		try {
			java.sql.Statement st = EasterEggs.c.createStatement();
			String sql = ("SELECT COUNT(*) FROM EasterEggsData;");
			ResultSet rs = st.executeQuery(sql);
			result = rs.getInt(1);
			rs.close();
			st.close();
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
		return result;
	}
	
	public static int countFoundedEasterEggs(Player p) {
		int result = 555;
		try {
			java.sql.Statement st = EasterEggs.c.createStatement();
			String sql = ("SELECT COUNT(uuid) FROM PlayerData WHERE uuid='" + p.getUniqueId().toString() + "';");
			ResultSet rs = st.executeQuery(sql);
			result = rs.getInt(1);
			rs.close();
			st.close();
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
		return result;
	}
	
	public static String getEasterMessage(Player p) {
		return ChatColor.translateAlternateColorCodes('&', "&aDu hast &6" + countFoundedEasterEggs(p) + " &a von &6" 
				+ countMaxEasterEggs() + " &a Ostereiern gefunden!");
	}

}
