package de.dieunkreativen.eastereggs;

import java.sql.SQLException;
import java.util.logging.Logger;

import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import de.dieunkreativen.eastereggs.util.SQLite;

public class EasterEggs extends JavaPlugin {
	Logger log;
	PluginDescriptionFile pdfFile = this.getDescription();
	SQLite DB = new SQLite(this.getDataFolder().toString() + "/data.db");
	public static java.sql.Connection c = null;
	static EasterEggs plugin;
	private final PlayerListener playerListener = new PlayerListener(this);
	
    @Override
    public void onLoad() {
            log = getLogger();
    }
	
	@Override
	public void onEnable() {
		plugin = this;
		saveDefaultConfig();
		
		if (!setupSQL() ) {
            log.severe(getDescription().getName() + " Database error!");
            getServer().getPluginManager().disablePlugin(this);
            return;
        }
		
		log.info(pdfFile.getName() + " version " + pdfFile.getVersion() + " is enabled!");
		PluginManager pm = this.getServer().getPluginManager();
		pm.registerEvents(playerListener, this);
		this.getCommand("easter").setExecutor(new CmdExecutor(this));
	}
	
	@Override
	public void onDisable() {
		log.info(pdfFile.getName() + " version " + pdfFile.getVersion() + " is disabled!");
	}
	
	public boolean setupSQL() {
		try {
			c = DB.openConnection();
			DBHandler.createTables();
		} catch (NullPointerException e) {
			return false;
		} catch (ClassNotFoundException e) {
			return false;
		} catch (SQLException e) {
			return false;
		}
		
		return true;
	}
	
	public static EasterEggs getSelf() {
		return plugin;
	}

}
