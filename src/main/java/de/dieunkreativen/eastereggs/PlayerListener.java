package de.dieunkreativen.eastereggs;

import java.util.HashMap;
import java.util.Map;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.block.Skull;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class PlayerListener implements Listener {
	EasterEggs plugin;
	Map<Player, Location > locations = new HashMap<Player, Location>();
	
	public PlayerListener(EasterEggs instance) {
		plugin = instance;
	}
	
	@EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
		Player p = event.getPlayer();
		if (event.getAction() == Action.LEFT_CLICK_BLOCK) {
			Block b = event.getClickedBlock();
			Location loc = b.getLocation();
			if (b.getType() == Material.SKULL) {
				if (p.getInventory().getItemInMainHand().getType() == Material.BLAZE_ROD && p.hasPermission("eastereggs.create")) {
					//Erstellen/bearbeiten eines Osterei'
					if (DBHandler.isEasterEgg(loc)) {
						p.openInventory(DBHandler.getEasterEggInventory(loc));
					}
					else {
						Inventory custominv = Bukkit.createInventory(null, 27, "Erstellen");
						locations.put(p, loc);
						p.openInventory(custominv);
					}
				}
				else if (DBHandler.isEasterEgg(loc) && DBHandler.hasOpened(p, loc) == false) {
					p.openInventory(DBHandler.getEasterEggInventory(loc));
					p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1F, 0);
					DBHandler.addOpenedEgg(p, loc);
					p.sendMessage(DBHandler.getEasterMessage(p));
				}
				else if (DBHandler.isEasterEgg(loc) && DBHandler.hasOpened(p, loc) == true) {
					p.playSound(p.getLocation(), Sound.BLOCK_CHEST_LOCKED, 1F, 0);
					p.sendMessage(ChatColor.RED + "Bereits ge�ffnet!");
				}
			}
		}
	}
	
	@EventHandler
    public void onInventoryClose(InventoryCloseEvent event) {
		Inventory inv = event.getInventory();
		Player p = (Player) event.getPlayer();
		if (inv.getName().equalsIgnoreCase("Erstellen")) {
			DBHandler.addEasterEgg(inv, locations.get(p));
			locations.remove(p);
		}
	}
	
	@EventHandler
	public void onBlockBreak (BlockBreakEvent event) {
		Player p = event.getPlayer();
		Location loc = event.getBlock().getLocation();
		if (DBHandler.isEasterEgg(loc) && p.getInventory().getItemInMainHand().getType() == Material.END_ROD && p.hasPermission("eastereggs.create")) {
			DBHandler.removeEasterEgg(loc);
			p.sendMessage(ChatColor.RED + "Osterei entfernt!");
		}
		else if (DBHandler.isEasterEgg(loc) || p.getInventory().getItemInMainHand().getType() == Material.BLAZE_ROD) {
			event.setCancelled(true);
		}
	}
	
	
	 public static boolean invIsEmpty(Inventory inv){
		 for(ItemStack item : inv.getContents()){
			 if(item != null){
				 if(item.getType() != Material.AIR){
					 return false;
				 }
			 }
		 }
		 return true;
	}

}
