package de.dieunkreativen.eastereggs;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdExecutor implements CommandExecutor{
	EasterEggs plugin;
	
	public CmdExecutor(EasterEggs instance) {
		plugin = instance;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender instanceof Player && cmd.getName().equalsIgnoreCase("easter")) {
			Player p = (Player)sender;
			p.sendMessage(DBHandler.getEasterMessage(p));
			return true;
		}
		return false;
	}
}
